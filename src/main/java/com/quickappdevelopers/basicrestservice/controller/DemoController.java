package com.quickappdevelopers.basicrestservice.controller;

import com.quickappdevelopers.basicrestservice.service.DataBaseService;
import com.quickappdevelopers.basicrestservice.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vasudev Jamwal on 5/6/2018.
 */

@RestController
public class DemoController {

    static final private String ROOT = "/";
    static final private String ITEMS = ROOT + "items";
    static final private String ITEM = ITEMS + "/{id}";

    
    @Autowired
    private DataBaseService dataBaseService;

    @RequestMapping(method = RequestMethod.GET, value = ITEMS)
    public List<Item> getAllItems() {
        return dataBaseService.getItems();
    }

    @RequestMapping(method = RequestMethod.GET, value = ROOT)
    public String helloMessage() {
        return "Hello World";
    }

    @RequestMapping(method = RequestMethod.GET, value = ITEM)
    public Item getItem(@PathVariable Integer id) {
        return dataBaseService.getItem(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = ITEMS)
    public String addItem(@RequestBody Item item) {
        dataBaseService.addItem(item);
        return "Successfully added";
    }

    @RequestMapping(method = RequestMethod.PUT, value = ITEM)
    public String updateItem(@RequestBody Item item, @PathVariable Integer id) {
        boolean result = dataBaseService.updateItem(id, item);
        return result ? "Successfully updated" : "Item not present";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = ITEM)
    public void deleteItem(@PathVariable Integer id) {
        dataBaseService.deleteItem(id);
    }
}

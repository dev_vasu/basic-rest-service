package com.quickappdevelopers.basicrestservice.model;

/**
 * Created by Vasudev Jamwal on 5/6/2018.
 */
public class Item {

    int propertyOne;
    String propertyTwo;

    public Item() {
    }

    public Item(int propertyOne, String propertyTwo) {
        this.propertyOne = propertyOne;
        this.propertyTwo = propertyTwo;
    }

    public int getPropertyOne() {
        return propertyOne;
    }

    public void setPropertyOne(int propertyOne) {
        this.propertyOne = propertyOne;
    }

    public String getPropertyTwo() {
        return propertyTwo;
    }

    public void setPropertyTwo(String propertyTwo) {
        this.propertyTwo = propertyTwo;
    }
}

package com.quickappdevelopers.basicrestservice.service;

import com.quickappdevelopers.basicrestservice.model.Item;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Vasudev Jamwal on 5/6/2018.
 */
@Service
public class DataBaseService {


    static List<Item> items = new ArrayList<>();

    static {
        items = new ArrayList<>();

        items.add(new Item(1, "Item 1"));
        items.add(new Item(2, "Item 2"));
        items.add(new Item(3, "Item 3"));
        items.add(new Item(4, "Item 4"));
        items.add(new Item(5, "Item 5"));
    }

    public List<Item> getItems() {
        return items;
    }

    public Item getItem(int id) {
        Item item = items.stream()
                .filter(i -> i.getPropertyOne() == id)
                .findFirst()
                .get();

        return item;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public boolean updateItem(int id, Item item) {
        final boolean[] result = {false};
        items.stream()
                .forEach(i -> {
                    if (i.getPropertyOne() == id) {
                        i.setPropertyTwo(item.getPropertyTwo());
                        result[0] = true;
                    }
                });
        return result[0];
    }

    public void deleteItem(Integer id) {
        items = items.stream()
                .filter(item -> {
                    return item.getPropertyOne() != id;
                })
                .collect(Collectors.toList());
    }
}
